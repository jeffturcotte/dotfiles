import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import System.IO
import qualified Data.Map as M
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Hooks.FadeInactive

mask = mod1Mask

myLayout = avoidStruts $ noBorders (
	Tall 1 (3/100) (1/2) |||
	Full
	)

myKeys x = M.union (keys defaultConfig x) (M.fromList 
	[((mask, xK_f), spawn "menu-programs")
	,((mask .|. shiftMask, xK_l), spawn "xscreensaver-command -lock")
	])

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
	where fadeAmount = 0.55

main = do 
	xmproc <- spawnPipe "xmobar"
	xmonad $ defaultConfig 
		{ modMask = mask
		, terminal = "urxvt"
		, keys = myKeys
		, layoutHook = myLayout
		, logHook = myLogHook <+> dynamicLogWithPP xmobarPP
			{ ppOutput = hPutStrLn xmproc
			, ppTitle = xmobarColor "#53b1f5" "" . shorten 120 
			}
		}
