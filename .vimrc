set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'

Bundle 'SuperTab-continued.'
Bundle 'kien/ctrlp.vim'
Bundle 'CSApprox'
Bundle 'Shougo/unite.vim'
"Bundle 'Lokaltog/vim-powerline'
Bundle 'Tabular'
Bundle 'ack.vim'
Bundle 'quickrun.vim'
Bundle 'scrooloose/nerdtree'
"Bundle 'scrooloose/syntastic'
"Bundle 'kogakure/vim-sparkup'
Bundle 'abolish.vim'

filetype plugin indent on

set encoding=utf-8
set autoread
set mouse=a
set clipboard=unnamed
set cmdheight=1
set laststatus=2
set ttyfast
set lazyredraw

syntax on

"set t_Co=256
"set gfn=Monaco:h12
set guioptions+=lrb
set guioptions-=lrb
set background=dark
colorscheme darkspectrum
hi SpecialKey guibg=NONE ctermbg=NONE

set list listchars=tab:┊\ ,nbsp:·,trail:·
set ruler
set tabstop=4 shiftwidth=4
set relativenumber
set linespace=2
set noexpandtab
set smartindent
set backspace=start,indent,eol
set nowrap
set title
set modeline
set showbreak=>\ 

" look for tags file in current directory first and work up the tree to root
set tags=tags;/

" backups
set noswapfile
set nobackup " make backup files
" set backupdir=~/.vim/backup " where to put backup files

" searching
set incsearch ignorecase smartcase showmatch hlsearch

" scrolling
set scrolloff=8         "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1

" Wild Menu
set wildmenu

" Allow unsaved hidden buffers
set hidden

" reselect visual block after indent/outdent
vnoremap < <gv
vnoremap > >gv

let mapleader=" "
nnoremap <silent> <Leader>, :e ~/.vimrc<CR>
nnoremap <Leader>j :CtrlPMixed<CR>
"nnoremap <Leader>j :Unite -no-split -start-insert file_rec<CR>
nnoremap <Leader>l :CtrlPBuffer<CR>
nnoremap <Leader>k :CtrlPBufTag<CR>
nnoremap <Leader>R :CtrlPClearAllCaches<CR>
nnoremap <silent> <Leader>[ :nohlsearch<CR>
nnoremap <silent> <Leader>n :NERDTreeToggle<CR>
nnoremap <silent> <Leader>r :QuickRun<CR>
" nnoremap <silent> <leader>e :Errors<CR>
"nnoremap <Leader>w :call WriteUpload()<CR>
vnoremap <silent> <Leader>a :Tabularize /=>\?<CR>
nnoremap <silent> <Leader>F Vgq
nnoremap <Leader>! :1,999bd<CR>
nnoremap <Leader>!! :1,999bd!<CR>

command! WW call TransmitUpload()
command! W call  RsyncUpload()

" ctrlp
let g:ctrlp_working_path_mode=0
let g:ctrlp_mruf_case_sensitive = 0
let g:ctrlp_extensions=['buffertag', 'mixed']
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ }
let g:ctrlp_by_filename = 1
let g:ctrlp_regexp = 0

" syntastic
"let g:syntastic_enable_signs=1
"let g:syntastic_auto_jump=1
"let g:syntastic_stl_format = '[%E{Err: %fe #%e}%B{, }%W{Warn: %fw #%w}]'
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

" fix sparkup/supertab conflict
let g:sparkupNextMapping = '<c-x>'

" fuzzyfinder
"let g:fuf_buffetag_ctagsPath='/usr/local/bin/ctags'
"let g:fuf_buffertag__css='--language-force=css'
"let g:fuf_buffertag__php='--language-force=php --php-types=cdf'
let g:fuf_coveragefile_exclude = '\v\~$|\.(o|exe|dll|bak|orig|swp|jpg|jpeg|png|gif)$|(^|[/\\])\.(hg|git|svn)($|[/\\])'

" PHP Indenting
let g:PHP_outdentphpescape=0
let g:PHP_useHTMLIndent=1

" Functions
fun! RsyncUpload()
	silent write
	call system("upload " . expand('%:p'))
	echo "File " . expand('%') . " saved and uploaded"
endfun

" Autocommands

" sparkup filetypes
augroup sparkup_types
   autocmd!
   autocmd FileType mustache,php runtime! ftplugin/html/sparkup.vim
augroup END

" reload vimrc
autocmd! BufWritePost .vimrc source %
